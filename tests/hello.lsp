;;; Lisp: SLOC=3 LLOC=0

;;; ================================================== ;;;
;;; =========== HELLO WORLD SIMULATION ============== ;;;
;;; ================================================== ;;;


;;; This function simply returns the string Hello World that is in quotes.

(DEFUN HELLO ()
  "HELLO WORLD"
  )
